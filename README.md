# VPN setup instructions

1. Connect to the OpenWrt2 network.
1. Open a browser and visit 192.168.1.1.
1. Log in to the admin page with username and password. Note: Password will be blank on the first login. 
1. Navigate to VPN -> LanBridge-Cient. On this page, you can download, delete, and re-generate configs.
1. Download config.
**Note: Download the TAP config for desktop and TUN config for mobile**

## Windows Client Setup
1. Download OpenVPN Cient at https://openvpn.net/community-downloads/. 
1. Follow the wizard to install the client. 
1. Launch OpenVPN GUI.
1. Select import file and find the config you downloaded.
1. Click connect to connect to your VPN.

## macOS Client Setup
1. Download tunnelblick at https://tunnelblick.net/cInstall.html.
1. Folow the wizard to install the client.
1. Drag and drop the config you downloaded in the tunneblick icon located in the top-right toolbar.
1. Left click on the tunnelblick icon and select connect.

## Linux Client Setup
1. Download OpenVPN Cient at https://openvpn.net/community-downloads/. 
1. Open a terminal and run the command sudo openvpn --config ./\<location of downloaded config\>/\<name of config\>.ovpn

## iOS/Android Client Setup
1. Download OpenVPN Connect in the Apple App Store or Google Play Store.
1. Open the app and import the TUN config you downloaded in the VPN setup.
1. Once successfully imported you tap the connect button in the list of profiles.

## To Compile

**Note: Before compiling install 'make' utility and development tools. You can find resources at https://openwrt.org/docs/guide-developer/build-system/install-buildsystem.**
1. Clone repo - git clone https://gitlab.com/slthomason/wg3526-vpn.git
1. cd /wg3526
1. Make a directory for the firmware - mkdir <path to store builds>
1. Compile - ./compile.sh wg3526-vpn
1. Copy bin - cp bin/targets/ramips/mt7621/openwrt-ramips-mt7621-zbtlink_zbt-wg3526-16m-squashfs-sysupgrade.bin <path to directory from step 3>/wg3526-vpn.bin
