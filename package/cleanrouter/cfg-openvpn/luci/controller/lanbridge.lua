module("luci.controller.lanbridge", package.seeall)

function index()
	entry( {"admin", "vpn", "lanbridge"}, template("lanbridge"), _("LanBridge-Client"), 99 )
	entry( {"admin", "vpn", "lanbridge", "list"}, call("action_list")).leaf = true
	entry( {"admin", "vpn", "lanbridge", "dload"}, call("action_dload")).leaf = true
	entry( {"admin", "vpn", "lanbridge", "delete"}, call("action_delete")).leaf = true
	entry( {"admin", "vpn", "lanbridge", "generate"}, call("action_generate")).leaf = true
	entry( {"admin", "vpn", "lanbridge", "genstatus"}, call("action_genstatus")).leaf = true
end

 dir = "/etc/openvpn/clients/"

function action_list()
	local output, itr, i
	local outarr = { }
	output = luci.sys.exec("/bin/ls -1 " .. dir .. "*.ovpn")
	output = string.gsub(output, dir, "")
	itr = 0
	for i in string.gmatch(output, "[^\r\n]+") do
		outarr[itr] = i
		itr = itr + 1
	end 
	luci.http.prepare_content("application/json")
	luci.http.write_json(outarr or {})
	
end

function action_delete(file)
	local pname = string.gsub(file, ".ovpn", "")
	luci.sys.exec("/usr/bin/easyrsa --pki-dir=/etc/easy-rsa/pki/ --batch revoke " .. pname)
	nixio.fs.unlink(dir .. file)
	luci.http.prepare_content("text/plain; charset=utf-8")
	luci.http.write_json("ok");
end

function action_dload(file)
	local output
	
	output = nixio.fs.readfile(dir .. file, 524288)
	luci.http.prepare_content("application/octet-stream")
	luci.http.write(output)
end

function action_generate(profile, remote, conntype)
	if remote.len == nil or remote == "" then
		remote = "auto"
	end
	if conntype == nil or conntype == "" then
		conntype = "tap"
	end
	luci.sys.exec("/bin/ovpn-gen-client " .. profile .. " " .. remote .. " " .. conntype)
	luci.http.prepare_content("text/plain; charset=utf-8")
	luci.http.write_json("ok");
end

function action_genstatus()
	local res = 0;
	local output;
	
	output = luci.sys.exec("/bin/ps w|/bin/grep ovpn-gen-client|/bin/grep -v grep");
	if output ~= "" then
		res = res + 1;
	end
	output = luci.sys.exec("/bin/ps w|/bin/grep ovpn-gen-server|/bin/grep -v grep");
	if output ~= "" then
		res = res + 1;
	end
	
	luci.http.prepare_content("application/json")
	luci.http.write_json(res or {})
end
